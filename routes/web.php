<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['role:Admin']], function () {
    
    //Administrador Alumnos
    Route::get('/alumnos', 'App\Http\Controllers\AlumnoController@index')->name('alumnos');
    Route::get('/alumnos/create', 'App\Http\Controllers\AlumnoController@create')->name('alumnos.create');
    Route::post('/alumnos', 'App\Http\Controllers\AlumnoController@store')->name('alumnos.store');
    
    Route::get('/alumnos/{id}/edit','App\Http\Controllers\AlumnoController@edit')->name('alumno.edit');
    Route::put('/alumnos/{id}','App\Http\Controllers\AlumnoController@update')->name('alumno.update');
    Route::delete('/alumnos/{id}','App\Http\Controllers\AlumnoController@destroy')->name('alumnos.destroy');


    //Administrador Escuelas
    Route::get('/escuelas/create', 'App\Http\Controllers\EscuelaController@create')->name('escuelas.create');
    Route::post('/escuelas', 'App\Http\Controllers\EscuelaController@store')->name('escuelas.store');
    
    Route::get('/escuelas/{id}/edit','App\Http\Controllers\EscuelaController@edit')->name('escuela.edit');
    Route::put('/escuelas/{id}','App\Http\Controllers\EscuelaController@update')->name('escuela.update');
    Route::delete('/escuelas/{id}','App\Http\Controllers\EscuelaController@destroy')->name('escuelas.destroy');

});
    
    //Alumno
    Route::group(['middleware' => ['role:Alumno']], function () {
        Route::get('/alumno', 'App\Http\Controllers\AlumnoController@indexalumno')->name('alumno');
        
        
    });
