@extends('layouts.app')

@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

<div class="container-fluid mt--7">
    <div class="row justify-content-center">
       <div class="col">
           <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h1 class="mb-0">Editar Alumnos</h1>
                    </div>
                </div>
            </div>

               <div class="card-body">
            
                    <form action="{{ route('alumno.update',$alumno->id ) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <h6 class="heading-small text-muted mb-4">{{ __('Información del alumno') }}</h6>

                        <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('Nombre') }}</label>
                            <input type="text" name="nombre" id="input-status" class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{$alumno->nombre}}">
    
                            @if ($errors->has('apellidoPaterno'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidoPaterno') }}</strong>
                                </span>
                            @endif
                        </div>
    
                        
                        <div class="form-group{{ $errors->has(' apellidos   ') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __(' Apellidos  ') }}</label>
                            <input type="text" name="apellidos" id="input-status" class="form-control form-control-alternative{{ $errors->has('apellidos ') ? ' is-invalid' : '' }}" placeholder="{{ __('apellidos ') }}" value="{{$alumno->apellidos}}">
    
                            @if ($errors->has('apellidos  '))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('apellidos  ') }}</strong>
                                </span>
                            @endif
                        </div>
    
                        <div class="form-group{{ $errors->has('  	fecha_de_nacimiento   ') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('  	Fecha de Nacimiento  ') }}</label>
                            <input type="date" name="fecha_de_nacimiento" id="input-status" class="form-control form-control-alternative{{ $errors->has(' 	fecha_de_nacimiento ') ? ' is-invalid' : '' }}" placeholder="{{ __(' 	fecha_de_nacimiento ') }}" value="{{$alumno->fecha_de_nacimiento}}">
    
                            @if ($errors->has(' 	fecha_de_nacimiento  '))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first(' 	fecha_de_nacimiento  ') }}</strong>
                                </span>
                            @endif
                        </div>
    
                        <div class="form-group{{ $errors->has('  	ciudad   ') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('  	Ciudad  ') }}</label>
                            <input type="text" name="ciudad" id="input-status" class="form-control form-control-alternative{{ $errors->has(' 	ciudad ') ? ' is-invalid' : '' }}" placeholder="{{ __(' 	ciudad ') }}" value="{{$alumno->ciudad}}">
    
                            @if ($errors->has(' 	ciudad  '))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first(' 	ciudad  ') }}</strong>
                                </span>
                            @endif
                        </div>
    
                        <div class="form-group{{ $errors->has('Escuela') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-email">{{ __('Escuela') }}</label>
                    <select name="escuela_id" class="form-control" id="" required>
                        @foreach($escuelas as $escuela)
                            <option value="{{$escuela->id}}">{{$escuela->nombre}}</option>
                        @endforeach    
                    </select>                        
                    @if ($errors->has('escuela_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('escuela_id') }}</strong>
                        </span>
                    @endif
                </div>
                       
                        <div class="form-group{{ $errors->has('  	role   ') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('  	Roles ') }}</label>
                            <select name="usuarios_id" id="" class="form-control"  required>
                                @foreach ($roles as $role)
                                <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has(' 	Role  '))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first(' 	role  ') }}</strong>
                                </span>
                            @endif
                        </div>
                        <br>
                        <div class="col-md-6 center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" style="background:#5e72e4 !important;" >Editar</button>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
