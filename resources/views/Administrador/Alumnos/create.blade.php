@extends('layouts.app')

@section('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
<div class=" container  container-fluid mt--7" style="">
    <div class="card bg-white shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <h1 class="mb-0">{{ __('Agregar Alumnos') }}</h1>
            </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('alumnos.store' ) }}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                @method('post')

                <h6 class="heading-small text-muted mb-4">{{ __('Ingrese los datos del alumno') }}</h6>
                
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                
                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Nombre del Alumno') }}</label>
                    <input type="text" min="0" name="nombre" id="input-nombre" class="form-control form-control-alternative{{ $errors->has('nombre') ? ' is-invalid' : '' }}" placeholder="{{ __('nombre') }}" value="" required>

                    @if ($errors->has('nombre'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Apellidos del Alumno') }}</label>
                    <input type="text" min="0" name="apellidos" id="input-apellidos" class="form-control form-control-alternative{{ $errors->has('apellidos') ? ' is-invalid' : '' }}" placeholder="{{ __('apellidos') }}" value="" required>

                    @if ($errors->has('apellidos'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('apellidos') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Fecha de Nacimiento del Alumno') }}</label>
                    <input type="date" min="0" name="fecha_de_nacimiento" id="input-fecha_de_nacimiento" class="form-control form-control-alternative{{ $errors->has('fecha_de_nacimiento') ? ' is-invalid' : '' }}" placeholder="{{ __('fecha_de_nacimiento') }}" value="" required>

                    @if ($errors->has('fecha_de_nacimiento'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('fecha_de_nacimiento') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Ciudad del Alumno') }}</label>
                    <input type="text" min="0" name="ciudad" id="input-ciudad" class="form-control form-control-alternative{{ $errors->has('ciudad') ? ' is-invalid' : '' }}" placeholder="{{ __('ciudad') }}" value="" required>

                    @if ($errors->has('ciudad'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('ciudad') }}</strong>
                        </span>
                    @endif
                </div>
                <br>
                <div class="form-group{{ $errors->has('Escuela') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-email">{{ __('Escuela') }}</label>
                    <select name="escuela_id" class="form-control" id="" required>
                        @foreach($escuelas as $escuela)
                            <option value="{{$escuela->id}}">{{$escuela->nombre}}</option>
                        @endforeach    
                    </select>                        
                    @if ($errors->has('escuela_id'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('escuela_id') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('  	role   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('  Rol quien creo el alumno  ') }}</label>
                        <select name="usuarios_id" id="" class="form-control" required>
                            @foreach ($roles as $role)
                            <option value="{{$role->id}}">{{$role->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has(' 	Role  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first(' 	role  ') }}</strong>
                            </span>
                        @endif
                    </div>



                    <br>
                    <div class="-6 col-md-6 center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Guardar') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
