@extends('layouts.app')

@section('content')

<div class="container container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class=" row align-items-center">
                        <div class=" col-8">
                            <h3 class="mb-0">Alumnos Inscritos</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('alumnos.create') }}" class="btn btn-sm btn-primary">Agregar Alumnos</a>
                        </div>
                    </div>
                </div>
                
                <div class="col-12">
                                        </div>

                <div class="container table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Apellidos</th>
                                <th scope="col">Fecha de Nacimiento</th>
                                <th scope="col">Ciudad</th>
                                <th scope="col">Escuela</th>
                                <th scope="col">Fecha Registro</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        @foreach ($alumnos as $orden=>$alumno)
                    <tbody>
                        <tr>
                <td>{{++$orden}}</td>
                <td>{{$alumno->nombre}}</td>
                <td>{{$alumno->apellidos}}</td> 
                <td>{{$alumno->fecha_de_nacimiento}}</td> 
                <td>{{$alumno->ciudad}}</td>   
                <td>{{$alumno->escuela_id}}</td>   
                <td>{{$alumno->created_at}}</td> 
                <td>
            
        
                                                    
                        <form action="{{route("alumnos.destroy", $alumno->id)}}" method="POST"> 
                            @method("DELETE")
                            @csrf
                            <a class="btn btn-primary" href="{{ route('alumno.edit', $alumno->id ) }}">Editar</a>
                        <button class="btn btn-secondary" type="submit">Eliminar</button>
                       
                        </form>
                
            
        </td>
                        </tr>
                    </tbody>

                    @endforeach
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{ $alumnos->links('pagination::bootstrap-5') }}
                        </nav>
                    </div>
            </div>
        </div>
        
    </div>
    
    </div>
</div>

<!---------------------- Escuelas ------------------------------>

<div class="container container-fluid mt--7">
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class=" row align-items-center">
                        <div class=" col-8">
                            <h3 class="mb-0">Escuelas Inscritas</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('escuelas.create') }}" class="btn btn-sm btn-primary">Agregar Escuelas</a>
                        </div>
                    </div>
                </div>
                
                <div class="col-12">
                                        </div>

                <div class="container table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col"># ID</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Dirección</th>
                                <th scope="col">Logotipo</th>
                                <th scope="col">Correo electrónico</th>
                                <th scope="col">Teléfono</th>
                                <th scope="col">Página web</th>
                                <th scope="col">Fecha de creacion</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        @foreach ($escuelas as $escuela)
                    <tbody>
                        <tr>
                <td>{{$escuela->id}}</td>
                <td>{{$escuela->nombre}}</td>
                <td>{{$escuela->direccion}}</td> 
                <td>
                <a class="avatar avatar-xs rounded-circle" data-toggle="tooltip" data-original-title="">
                     <img alt="Image" class="img-fluid avatar-xs rounded-circle" src="{{ asset('logotipo/'. $escuela->logotipo ) }}">
                </a>
                </td>
                <td>{{$escuela->correo_electronico}}</td>   
                <td>{{$escuela->telefono}}</td> 
                <td>{{$escuela->pagina_web}}</td>  
                <td>{{$escuela->created_at}}</td> 
                <td>
            
        
                                                    
            <form action="{{route("escuelas.destroy", $escuela->id)}}" method="POST"> 
                @method("DELETE")
                @csrf
                <a class="btn btn-primary" href="{{ route('escuela.edit', $escuela->id ) }}">Editar</a>
            <button class="btn btn-secondary" type="submit">Eliminar</button>
           
            </form>
    

</td>
            </tr>
                    </tbody>

                    @endforeach
                    </table>
                </div>
                <div class="card-footer py-4">
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{ $escuelas->links('pagination::bootstrap-5') }}
                        </nav>
                    </div>
            </div>
        </div>
        
    </div>
    
    </div>
</div>

<br>

@endsection
