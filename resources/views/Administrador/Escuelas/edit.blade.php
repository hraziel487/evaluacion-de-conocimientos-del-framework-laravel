@extends('layouts.app')

@section('content')

<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>

<div class="container-fluid mt--7">
    <div class="row justify-content-center">
       <div class="col">
           <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-8">
                        <h1 class="mb-0">Editar Escuela</h1>
                    </div>
                </div>
            </div>

               <div class="card-body">
            
                    <form action="{{ route('escuela.update',$escuela->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <h6 class="heading-small text-muted mb-4">{{ __('Información del escuela') }}</h6>

                        <div class="form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('Nombre') }}</label>
                            <input type="text" name="nombre" id="input-status" class="form-control form-control-alternative{{ $errors->has('status') ? ' is-invalid' : '' }}" placeholder="{{ __('Nombre') }}" value="{{$escuela->nombre}}">
    
                            @if ($errors->has('nombre'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                </span>
                            @endif
                        </div>
                      
    
                        <div class="form-group{{ $errors->has('  	direccion   ') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('  	Direccion  ') }}</label>
                            <input type="text" name="direccion" id="input-status" class="form-control form-control-alternative{{ $errors->has(' 	direccion ') ? ' is-invalid' : '' }}" placeholder="{{ __(' 	direccion ') }}" value="{{$escuela->direccion}}">
    
                            @if ($errors->has(' 	direccion  '))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first(' 	direccion  ') }}</strong>
                                </span>
                            @endif
                        </div>

                        
                        <div class="form-group">
                            <label class="form-control-label" for="input-logotipo">Logotipo</label>
                            <input type="file" name="logotipo" id="input-logotipo" class="form-control form-control-alternative" placeholder="{{ __('logotipo') }}" required>
                        </div>

                        <div class="form-group{{ $errors->has('  	correo_electronico   ') ? ' has-danger' : '' }}">
                            <label class="form-control-label" for="input-status">{{ __('  	correo_electronico  ') }}</label>
                            <input type="email" name="correo_electronico" id="input-status" class="form-control form-control-alternative{{ $errors->has(' 	correo_electronico ') ? ' is-invalid' : '' }}" placeholder="{{ __(' 	correo_electronico ') }}" value="{{$escuela->correo_electronico}}">
    
                            @if ($errors->has(' 	correo_electronico  '))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first(' 	correo_electronico  ') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Telefono') }}</label>
                    <input type="number" min="0" name="telefono" id="input-telefono" class="form-control form-control-alternative{{ $errors->has('telefono') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefono') }}" value="{{$escuela->telefono}}">

                    @if ($errors->has('telefono'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Pagina web') }}</label>
                    <input type="text" min="0" name="pagina_web" id="input-pagina_web" class="form-control form-control-alternative{{ $errors->has('pagina_web') ? ' is-invalid' : '' }}" placeholder="{{ __('Pagina web') }}" value="{{$escuela->pagina_web}}">

                    @if ($errors->has('pagina_web'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pagina_web') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('  	role   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('  		Quien va a administrar esta escuela  ') }}</label>
                        <select name="usuarios_id" id="" class="form-control" required>
                            <option value="1">Admin</option default>
                        </select>
                        @if ($errors->has(' 	Role  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first(' 	role  ') }}</strong>
                            </span>
                        @endif
                    </div>
                        <br>
                        <div class="col-md-6 center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block" style="background:#5e72e4 !important;" >Editar</button>
                        </div>
                    </form>     
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
