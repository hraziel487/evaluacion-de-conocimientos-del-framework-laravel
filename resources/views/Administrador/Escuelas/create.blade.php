@extends('layouts.app')

@section('content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
<div class=" container  container-fluid mt--7" style="">
    <div class="card bg-white shadow">
        <div class="card-header bg-white border-0">
            <div class="row align-items-center">
                <h1 class="mb-0">{{ __('Agregar Escuelas') }}</h1>
            </div>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('escuelas.store' ) }}" autocomplete="off" enctype="multipart/form-data">
                @csrf
                @method('post')

                <h6 class="heading-small text-muted mb-4">{{ __('Ingrese los datos de la escuela') }}</h6>
                
                @if (session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('status') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                
                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Nombre de la Escuela') }}</label>
                    <input type="text" min="0" name="nombre" id="input-nombre" class="form-control form-control-alternative{{ $errors->has('nombre') ? ' is-invalid' : '' }}" placeholder="{{ __('nombre') }}" value="" required>

                    @if ($errors->has('nombre'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nombre') }}</strong>
                        </span>
                    @endif
                </div>


                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Direccion de la Escuela') }}</label>
                    <input type="text" min="0" name="direccion" id="input-direccion" class="form-control form-control-alternative{{ $errors->has('direccion') ? ' is-invalid' : '' }}" placeholder="{{ __('direccion') }}" value="" required>

                    @if ($errors->has('direccion'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('direccion') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                            <label class="form-control-label" for="input-logotipo">Logotipo</label>
                            <input type="file" name="logotipo" id="input-logotipo" class="form-control form-control-alternative" placeholder="{{ __('logotipo') }}" required>
                        </div>


                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Correo electronico') }}</label>
                    <input type="email" min="0" name="correo_electronico" id="input-correo_electronico" class="form-control form-control-alternative{{ $errors->has('correo_electronico') ? ' is-invalid' : '' }}" placeholder="{{ __('Correo electronico') }}" value="" required>

                    @if ($errors->has('correo_electronico'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('correo_electronico') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Telefono') }}</label>
                    <input type="number" min="0" name="telefono" id="input-telefono" class="form-control form-control-alternative{{ $errors->has('telefono') ? ' is-invalid' : '' }}" placeholder="{{ __('Telefono') }}" value="" required>

                    @if ($errors->has('telefono'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('telefono') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('alumnos') ? ' has-danger' : '' }}">
                    <label class="form-control-label" for="input-alumnos">{{ __('Pagina web') }}</label>
                    <input type="text" min="0" name="pagina_web" id="input-pagina_web" class="form-control form-control-alternative{{ $errors->has('pagina_web') ? ' is-invalid' : '' }}" placeholder="{{ __('Pagina web') }}" value="" required>

                    @if ($errors->has('pagina_web'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pagina_web') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('  	role   ') ? ' has-danger' : '' }}">
                        <label class="form-control-label" for="input-status">{{ __('  		Quien va a administrar esta escuela  ') }}</label>
                        <select name="usuarios_id" id="" class="form-control" required>
                            <option value="1">Admin</option default>
                        </select>
                        @if ($errors->has(' 	Role  '))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first(' 	role  ') }}</strong>
                            </span>
                        @endif
                    </div>


                    <br>
                    <div class="-6 col-md-6 center">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">{{ __('Guardar') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
