<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Alumno;

class AlumnoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertAlumno(1,'Raul','hernandez','1999-08-01','Reus',5,1); 
        $this->insertAlumno(2,'Raciel','hernandez','1999-08-02','Reus',5,1);
        $this->insertAlumno(3,'Renata','hernandez','1999-08-03','Reus',1,1);
        $this->insertAlumno(4,'Rene','hernandez','1999-08-04','Reus',2,1);
        $this->insertAlumno(5,'Rambo','hernandez','1999-08-05','Reus',2,1);
        $this->insertAlumno(6,'Rius','hernandez','1999-08-06','Reus',4,1);
        $this->insertAlumno(7,'Luis','hernandez','1999-08-07','Reus',4,1);
        $this->insertAlumno(8,'Juan','hernandez','1999-08-08','Reus',9,1);
        $this->insertAlumno(9,'Rudy','hernandez','1999-08-09','Reus',3,1);
        $this->insertAlumno(10,'Richie','hernandez','1999-08-10','Reus',3,1);
        $this->insertAlumno(11,'Raquel','hernandez','1999-08-11','Reus',6,1);
        $this->insertAlumno(12,'Ramses','hernandez','1999-08-12','Reus',7,1);
        $this->insertAlumno(13,'Rolando','hernandez','1999-08-13','Reus',8,1);
        $this->insertAlumno(14,'Rosa','hernandez','1999-08-14','Reus',9,1);
    }
    private function insertAlumno($id,$nombreAlumno,$apellidoAlumno,$fecha_de_nacimiento,$ciudad,$escuela_id,$usuarios_id ){
    $Alumnos = new Alumno();
    $Alumnos->id = $id;
    $Alumnos->nombre = $nombreAlumno;
    $Alumnos->apellidos = $apellidoAlumno;
    $Alumnos->fecha_de_nacimiento = $fecha_de_nacimiento;
    $Alumnos->ciudad = $ciudad;
    $Alumnos->escuela_id = $escuela_id;
    $Alumnos->usuarios_id = $usuarios_id;
    $Alumnos->save();
    }
}
