<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Escuela;
class EscuelaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertEscuela(1,'Universidad de Barcelona','Gran Via de les Corts Catalanes, 585, 08007 Barcelona',"foto1.png",'universidad1@gmail.com','934 02 11 00','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(2,'Universidad de Tarragona','Gran Via de Tarragona, 585, 08007 Tarragona',"foto2.png",'universidad2@gmail.com','934 02 11 01','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(3,'Universidad de Castilla','Gran Via de les Corts Catalanes, 585, 08007 Castilla',"foto3.png",'universidad3@gmail.com','934 02 11 02','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(4,'Universidad de Leon','Gran Via de les Corts Catalanes, 585, 08007 Leon',"foto4.png",'universidad4@gmail.com','934 02 11 03','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(5,'Universidad de Valencia','Gran Via de les Corts Catalanes, 585, 08007  Valencia',"foto5.png",'universidad5@gmail.com','934 02 11 04','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(6,'Universidad de Sevilla','Gran Via de les Corts Catalanes, 585, 08007 Sevilla',"foto6.png",'universidad6@gmail.com','934 02 11 05','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(7,'Universidad de La mancha','Gran Via de les Corts Catalanes, 585, 08007 La mancha',"foto7.png",'universidad7@gmail.com','934 02 11 06','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(8,'Universidad de Granada','Gran Via de les Corts Catalanes, 585, 08007 Granada',"foto8.png",'universidad8@gmail.com','934 02 11 07','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(9,'Universidad de Valladolid','Gran Via de les Corts Catalanes, 585, 08007 Valladolid',"foto9.png",'universidad9@gmail.com','934 02 11 08','https://www.ub.edu/web/portal/ca/',1); 
        $this->insertEscuela(10,'Universidad de Girona','Gran Via de les Corts Catalanes, 585, 08007 Girona',"foto10.png",'universidad10@gmail.com','934 02 11 09','https://www.ub.edu/web/portal/ca/',1); 

    }
    private function insertEscuela($id,$nombreEscuela,$direccionEscuela,$logotipo,$correo_electronico,$telefono,$pagina_web,$usuarios_id ){
        $Escuelas = new Escuela();
        $Escuelas->id = $id;
        $Escuelas->nombre = $nombreEscuela;
        $Escuelas->direccion = $direccionEscuela;
        $Escuelas->logotipo = $logotipo;
        $Escuelas->correo_electronico = $correo_electronico;
        $Escuelas->telefono = $telefono;
        $Escuelas->pagina_web = $pagina_web;
        $Escuelas->usuarios_id = $usuarios_id;
        $Escuelas->save();
    }

}
