<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Guia de instalacion 

Requisitos 

    BD Mysql
    Proyecto realizado en laravel 9
    

Agregar el primer comando
    *composer install 

Modificar archivo .env
    El archivo llamado .env.example se elimina el .example y se agrega el nombre de su BD

Ejecutar nueva API key
    $php artisan key:generate

Finalmente el proyecto cuenta con seeders por ello es necesario ejecutarlos con el siguiente comando
    $ php artisan migrate --seed

Para ejecutar el programa se tienen que agregar 2 comandos para funcionar correctamente
    npm run dev
    php artisan serve
**Ambos comandos tienen que ejecutarse al mismo tiempo
