<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Alumno;
use App\Models\Escuela;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;


class AlumnoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alumnos = Alumno::paginate(5);
        $escuelas = Escuela::paginate(5);
        return view('Administrador.index',  compact('alumnos','escuelas'));

    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $alumnos = Alumno::all();
        $escuelas = Escuela::all();
        $roles = Role::all();
        return view("Administrador.Alumnos.create", compact('escuelas','alumnos','roles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alumnos = Alumno::create($request->all());
        if($request->has('password')){
            $alumnos->password = Hash::make($request->password);
        } 
        $alumnos->save();
        return redirect()->route('alumnos')->with('flash','Su usuario ha sido guardado satisfactoriamente.');
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alumno = Alumno::findOrFail($id);
        $roles = Role::all();
        $escuelas = Escuela::all();
        return view('Administrador.Alumnos.edit', compact('alumno','roles','escuelas'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alumnos =  Alumno::findOrFail($id);
        $alumnos->fill($request->all());
        if($request->has('password')){
            $alumnos->password = Hash::make($request->password);
        } 
        $alumnos->save();
        return redirect()->route('alumnos')->with('flash','Su usuario ha sido actualizado con Exito.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alumnos = Alumno::findOrFail($id);
        $alumnos->delete();
        return redirect()->route('alumnos')->with('flash','Su usuario ha sido eliminado con Exito.');

    }
}
