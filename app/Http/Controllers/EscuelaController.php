<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Escuela;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class EscuelaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $escuelas = Escuela::all();
        $roles = Role::all();
        return view("Administrador.Escuelas.create", compact('escuelas','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
            $rules = [
                'nombre' => 'required',
                'direccion' => 'required',
                'logotipo' => 'mimes:jpeg,png,jpg:|max:2048',
            ];
        
            $messages = [
                'nombre.required' => 'Es necesario agregar una direccion',
                'direccion.required' => 'Es necesario escribir una direccion',
            ];
            $validator = Validator::make($request->all(),$rules, $messages);
        
            if ($validator->fails()) {
                return redirect('escuelas/create')
                            ->withErrors($validator)
                            ->withInput();
            }
            $escuelas = Escuela::create($request->all());
            if($request->hasfile('logotipo')){
                $logotipo = $request->file('logotipo');
                $extension = $logotipo->getClientOriginalExtension();
                $nombre = time() . '.' . $extension;
                $logotipo->move('logotipo/' , $nombre);
                
                $escuelas->logotipo=$nombre;
            }
            $escuelas->save();
            return redirect()->route('alumnos')->with('flash','Su escuela ha sido guardado satisfactoriamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $escuela = Escuela::findOrFail($id);
        return view('Administrador.Escuelas.edit', compact('roles','escuela'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nombre' => 'required',
            'direccion' => 'required',
            'logotipo' => 'mimes:jpeg,png,jpg:|max:2048',
        ];
    
        $messages = [
            'nombre.required' => 'Es necesario agregar una direccion',
            'direccion.required' => 'Es necesario escribir una direccion',
        ];
        $validator = Validator::make($request->all(),$rules, $messages);
    
        if ($validator->fails()) {
            return redirect('escuelas/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $escuelas = Escuela::findOrFail($id);
        $escuelas->fill($request->all());
         if($request->hasfile('logotipo')){
                $logotipo = $request->file('logotipo');
                $extension = $logotipo->getClientOriginalExtension();
                $nombre = time() . '.' . $extension;
                $logotipo->move('logotipo/' , $nombre);
                
                $escuelas->logotipo=$nombre;
            }
        $escuelas->save();
        return redirect()->route('alumnos')->with('flash','Su usuario ha sido actualizado con Exito.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $escuelas = Escuela::findOrFail($id);
        $escuelas->delete();
        return redirect()->route('alumnos')->with('flash','Su usuario ha sido eliminado con Exito.');
    }
}
