<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    protected $table = "escuelas";

    protected $fillable = [
        'nombre',
        'direccion',
        'logotipo',
        'correo_electronico',
        'telefono',
        'pagina_web',
        'usuarios_id',
    ];

    protected $hidden = ['id'];

    public function users()
    {
        return $this->belongsTo(User::class,'usuarios_id','id');
    }

}
