<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    use HasFactory;

    protected $table = "alumnos";

    protected $fillable = [
        'nombre',
        'apellidos',
        'fecha_de_nacimiento',
        'ciudad',
        'escuela_id',
        'usuarios_id'
    ];

    protected $hidden = ['id'];

    public function users()
    {
        return $this->belongsTo(User::class,'usuarios_id','id');
    }

}
